# README #

## Zadania ##

* **Znalezienie pomysłu na aplikację**
* Analiza pomysłu
* Opracowanie klas
* Podział pracy
* Implementacja
* Testy
* Release

## Jak skonfigurować VCS? ##

* To skomplikowane. Wszystko dalej, dalej, dalej…
* Przy wyborze importowanych projektów trzeba odhaczyć Java

## Zasady udziału ##

* Praca na własnej gałęzi/gałęziach (**nie** master!)
* Zmienne wejściowe funkcji zaczynające się od twardej spacji
```
#!java

void function(String _input) {
System.out.println(_input);
}
```
* Zmienne nazywane po angielsku
* Duża ilość komentarzy w j. polskim/angielskim

## Uwagi? ##
Mailem, fejsem, smsem, tradycyjnie…