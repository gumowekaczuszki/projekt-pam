package pl.wroc.ue.pam.projekt;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import pl.wroc.ue.pam.projekt.adapters.TermAdapter;
import pl.wroc.ue.pam.projekt.datamodel.Context;
import pl.wroc.ue.pam.projekt.datamodel.Term;

public class MainActivity extends Activity {
    private Context defaultContext;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        // IMPORTANT: Create app context
        // It's gonna be used to pass data between activities
        defaultContext = new Context();

        // Fill the listview with terms data
        ListView lv = (ListView)findViewById(R.id.termList);
        adapter = new TermAdapter(this, defaultContext.getTerms(), defaultContext);
        lv.setAdapter(adapter);

        // OnItemClick setting
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(MainActivity.this, TermViewActivity.class);
                intent.putExtra("term", defaultContext.getTerms().get(i));
                startActivity(intent);
            }
        });
    }

    private TermAdapter adapter;

    // Add icons to action bar
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_activity_actions, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onResume() {
        adapter.notifyDataSetChanged();
        super.onResume();
    }

    // Add handlers for action bar
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case R.id.action_add:
                addTermDialog();
                break;
            case R.id.calc_term:
                // TODO: calc term mean
                break;
            case R.id.about_app:
                // TODO: about dialog
                break;
            default:
                return false;
        }
        return true;
    }

    private void addTermDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle(R.string.add);
        builder.setMessage(R.string.enter_term_name);

        final EditText input = new EditText(MainActivity.this);
        input.setSingleLine();
        builder.setView(input);

        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                // Add term
                defaultContext.getTerms().add(new Term(input.getText().toString()));
                // Update listview
                adapter.notifyDataSetChanged();
            }
        });
        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                // Do nothing
            }
        });

        builder.show();

        // Set focus on EditText
        input.requestFocus();
        // Show soft keyboard
        InputMethodManager imm = (InputMethodManager) getSystemService(android.content.Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
    }

}
