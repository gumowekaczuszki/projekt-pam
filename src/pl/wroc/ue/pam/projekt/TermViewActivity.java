package pl.wroc.ue.pam.projekt;

import android.app.Activity;
import android.app.DialogFragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import pl.wroc.ue.pam.projekt.adapters.CourseAdapter;
import pl.wroc.ue.pam.projekt.datamodel.Course;
import pl.wroc.ue.pam.projekt.datamodel.CourseList;
import pl.wroc.ue.pam.projekt.datamodel.Term;
import pl.wroc.ue.pam.projekt.fragments.CourseEditDialog;

import java.util.List;

public class TermViewActivity extends Activity implements CourseEditDialog.CourseDialogListener {
    private Term currentTerm = null;
    private CourseAdapter adapter = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Get term data
        Intent intent = getIntent();
        currentTerm = intent.getParcelableExtra("term");

        setContentView(R.layout.termviewactivity);

        // Set action bar to term name
        if (getActionBar() != null)
            getActionBar().setTitle(currentTerm.getTermName());

        // Fill list view with courses
        ListView lv = (ListView)findViewById(R.id.courseList);


        adapter = new CourseAdapter(this, CourseList.allCourses, currentTerm);
        lv.setAdapter(adapter);

        /*
        final ArrayAdapter<Course> myAdapter = new ArrayAdapter<Course>(this, android.R.layout.simple_list_item_1, CourseList.allCourses);
        myAdapter.add(new Course("aaa", 3, 3));
        myAdapter.add(new Course("bb", 3, 3));
        myAdapter.add(new Course("cc", 3, 3));
        lv.setAdapter(myAdapter);
        */

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                DialogFragment ced = CourseEditDialog.newInstance(CourseList.allCourses.get(i));
                ced.show(getFragmentManager().beginTransaction(), "dialog");
            }
        });

    }

    @Override
    public void onDeleteClick(DialogFragment dialog, Course course) {
        currentTerm.deleteCourse(course);
        adapter.notifyDataSetChanged();
    }
}