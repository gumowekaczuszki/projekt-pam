package pl.wroc.ue.pam.projekt.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import pl.wroc.ue.pam.projekt.R;
import pl.wroc.ue.pam.projekt.datamodel.Course;
import pl.wroc.ue.pam.projekt.datamodel.Term;

import java.util.ArrayList;

public class CourseAdapter extends ArrayAdapter<Course> {
    private Term term = null;

    public CourseAdapter(Context context, ArrayList<Course> courses, Term term) {
        super(context, 0, courses);
        this.term = term;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Course course = getItem(position);
        if (convertView == null)
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.courselistitem, parent, false);

        convertView.setTag(position);

        TextView courseName = (TextView)convertView.findViewById(R.id.courseName1);
        TextView ectsValue = (TextView)convertView.findViewById(R.id.ectsValue);
        TextView gradeValue = (TextView)convertView.findViewById(R.id.gradeValue);

        courseName.setText(course.getName());
        ectsValue.setText(String.valueOf(course.getEctspoints()));
        gradeValue.setText(String.valueOf(course.getGrade()));

        return convertView;
    }
}
