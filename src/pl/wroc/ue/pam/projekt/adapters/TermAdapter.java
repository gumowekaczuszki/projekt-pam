package pl.wroc.ue.pam.projekt.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.*;
import pl.wroc.ue.pam.projekt.R;
import pl.wroc.ue.pam.projekt.datamodel.Term;

import java.util.ArrayList;

public class TermAdapter extends ArrayAdapter<Term> {

    private pl.wroc.ue.pam.projekt.datamodel.Context defaultContext = null;

    // So far it may be omitted
    // If you encounter any problem, try uncommenting the following method
    /*
    public TermAdapter(Context context, ArrayList<Term> terms) {
        super(context, 0, terms);
    }
    */

    public TermAdapter(Context context, ArrayList<Term> terms, pl.wroc.ue.pam.projekt.datamodel.Context defaultContext) {
        super(context, 0, terms);
        this.defaultContext = defaultContext;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Term term = getItem(position);
        if (convertView == null)
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.termlistitem, parent, false);

        convertView.setTag(position);

        TextView tvTermName = (TextView) convertView.findViewById(R.id.termName);
        TextView tvCoursesList = (TextView) convertView.findViewById(R.id.termCourseList);
        ImageView ivItemPopup = (ImageView) convertView.findViewById(R.id.button_popup);

        tvTermName.setText(term.getTermName());
        tvCoursesList.setText(term.getStringOfCourses());
        ivItemPopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Prepare id of item
                LinearLayout ll = (LinearLayout) view.getParent();
                int pos = ((Integer) ll.getTag());
                // Show popup
                showPopup(view, pos);
            }
        });
        return convertView;
    }

    private int clickedListItem;

    private void showPopup(View view, int listPos) {
        // Move popup to make it available for listener
        clickedListItem = listPos;
        // Prepare popup
        PopupMenu popup = new PopupMenu(getContext(), view);
        popup.getMenuInflater().inflate(R.menu.term_popup, popup.getMenu());
        popup.setOnMenuItemClickListener(popup_menu_listener);
        // Finally show popup
        popup.show();
    }

    // Listener for popup menu clicks
    PopupMenu.OnMenuItemClickListener popup_menu_listener = new PopupMenu.OnMenuItemClickListener() {
        @Override
        public boolean onMenuItemClick(MenuItem menuItem) {
            switch (menuItem.getItemId()) {
                case R.id.term_popup_edit:
                    // Show dialog for editing
                    createEditDialog();
                    break;
                case R.id.term_popup_delete:
                    // Show dialog for deleting
                    deleteTermDialog();
                    break;
                default:
                    return false;
            }
            return true;
        }
    };

    // Dialog for editing term name
    private void createEditDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(R.string.edit);
        builder.setMessage(R.string.enter_term_name);

        final EditText input = new EditText(getContext());
        // Put EditText in the dialog
        builder.setView(input);
        // Get Term data clicked
        final Term term = defaultContext.getTerms().get(clickedListItem);
        // Limit to one line
        input.setSingleLine();
        // Put current name in the EditText
        input.setText(term.getTermName());
        // Select the name
        input.selectAll();

        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                // Change name
                term.setTermName(input.getText().toString());
                // Update listview
                notifyDataSetChanged();
            }
        });
        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                // Do nothing
            }
        });

        // Show dialog
        builder.show();
        // Set focus on EditText
        input.requestFocus();
        // Show soft keyboard
        InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
    }


    private void deleteTermDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(R.string.delete)
                .setMessage(getContext().getString(R.string.are_you_sure_delete)
                        + defaultContext.getTerms().get(clickedListItem).getTermName()
                        + "?");
        builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                // Delete term
                defaultContext.getTerms().remove(clickedListItem);
                // Update list view
                notifyDataSetChanged();
            }
        });
        builder.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                // Do nothing
            }
        });
        builder.show();
    }
}

