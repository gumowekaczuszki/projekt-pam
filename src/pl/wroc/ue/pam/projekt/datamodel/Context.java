package pl.wroc.ue.pam.projekt.datamodel;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public final class Context implements Parcelable {
    public Context() {
        terms = new ArrayList<Term>();
        // TODO: Load data


        // filling it nicely with data
        terms.add(new Term("First term"));
        terms.add(new Term("Second term"));
        terms.add(new Term("Third term"));
        terms.get(0).addCourse(new Course("Jezyki modelowania procesow biznesowych", 5.0f, 4));
        terms.get(0).addCourse(new Course("Zarzadzanie projektami IT", 4.0f, 5));
        terms.get(1).addCourse(new Course("Modelowanie baz danych", 4.5f, 4));
        terms.get(1).addCourse(new Course("Jezyk niemiecki", 3.0f, 1));
    }

    private ArrayList<Term> terms;

    public ArrayList<Term> getTerms() {
        return terms;
    }

    protected Context(Parcel in) {
        if (in.readByte() == 0x01) {
            terms = new ArrayList<Term>();
            in.readList(terms, Term.class.getClassLoader());
        } else {
            terms = null;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (terms == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(terms);
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Context> CREATOR = new Parcelable.Creator<Context>() {
        @Override
        public Context createFromParcel(Parcel in) {
            return new Context(in);
        }

        @Override
        public Context[] newArray(int size) {
            return new Context[size];
        }
    };
}
