package pl.wroc.ue.pam.projekt.datamodel;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class Course implements Parcelable {

    //public static ArrayList<Course> allCourses = new ArrayList<Course>();
    private String name;
    private float grade;
    private int ectspoints;

    public Course(String _name, float _grade, int _ectspoints) throws IllegalArgumentException {

        //allCourses = new ArrayList<Course>();

        setName(_name);
        setGrade(_grade);
        setEctspoints(_ectspoints);

        CourseList.addCourse(this);
    }



    // Getters, setters
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getGrade() {
        return grade;
    }

    public void setGrade(float grade) {
        if((grade % 0.5) != 0) // Check if grade is divisible by 0.5
            throw new IllegalArgumentException("Grade should be divisible by 0.5");
        if(grade < 2.0f || grade > 5.5f) // Check if grade is between 2.0 and 5.5
            throw new IllegalArgumentException("Grade needs to be between 2.0 and 5.5");
        this.grade = grade;
    }

    public int getEctspoints() {
        return ectspoints;
    }

    public void setEctspoints(int ectspoints) {
        if(ectspoints < 0) // Check if ECTS points are positive
            throw new IllegalArgumentException("ECTS points can be positive only");
        this.ectspoints = ectspoints;
    }

    // Methods
    public float weightedGrade() {
        return grade * ectspoints;
    }

    protected Course(Parcel in) {
        name = in.readString();
        grade = in.readFloat();
        ectspoints = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeFloat(grade);
        dest.writeInt(ectspoints);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Course> CREATOR = new Parcelable.Creator<Course>() {
        @Override
        public Course createFromParcel(Parcel in) {
            return new Course(in);
        }

        @Override
        public Course[] newArray(int size) {
            return new Course[size];
        }
    };
}
