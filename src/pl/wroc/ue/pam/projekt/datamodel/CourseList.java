package pl.wroc.ue.pam.projekt.datamodel;

import java.util.ArrayList;

/**
 * Created by Szynek on 2015-01-29.
 */
public class CourseList {
    public static ArrayList<Course> allCourses = new ArrayList<Course>();

    public static void addCourse(Course _course){
        allCourses.add(_course);
    }
}
