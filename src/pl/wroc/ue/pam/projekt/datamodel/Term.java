package pl.wroc.ue.pam.projekt.datamodel;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class Term implements Parcelable {
    // Variables
    private String termName;

    public ArrayList<Course> getCourseList() {
        return courseList;
    }

    private ArrayList<Course> courseList;

    // Constructors
    public Term(String _termName) {
        termName = _termName;
        courseList = new ArrayList<Course>();
    }

    // Getters, setters
    public String getTermName() {
        return termName;
    }

    public void setTermName(String termName) {
        this.termName = termName;
    }

    // Add a course
    public void addCourse(Course _addedCourse) {
        courseList.add(_addedCourse);
    }

    // Delete a course
    public boolean deleteCourse(Course _deletedCourse)
    {
        return courseList.remove(_deletedCourse);
    }

    // Count arithmetic mean
    public float arithmeticMean() {
        if(courseList.size() < 1)
            throw new UnsupportedOperationException("There are no courses. You cannot calculate a mean");

        // Sum all grades
        float gradesSum = 0;
        for (Course course : courseList) {
            gradesSum += course.getGrade();
        }

        // Divide the sum by number of courses and return
        return gradesSum/courseList.size();
    }

    // Count weighted mean
    public float weightedMean() {
        if(courseList.size() < 1)
            throw new UnsupportedOperationException("There are no courses. You cannot calculate a mean");

        float weightedGradesSum = 0;
        int weightsSum = 0;

        // Sum all weighted grades and weights
        for (Course course : courseList) {
            weightedGradesSum += course.weightedGrade();
            weightsSum += course.getEctspoints();
        }

        // Return mean - divided sum of weighted grades by sum of weights
        return weightedGradesSum/weightsSum;
    }

    public String getStringOfCourses()
    {
        if (courseList.size() == 0)
            return "";
        String list = courseList.get(0).getName();
        for (int i = 1; i < courseList.size(); i++) {
            Course course = courseList.get(i);
            list = list + ", " + course.getName();
        }
        return list;
    }

    @Override
    public String toString() {
        return getTermName();
    }

    protected Term(Parcel in) {
        termName = in.readString();
        if (in.readByte() == 0x01) {
            courseList = new ArrayList<Course>();
            in.readList(courseList, Course.class.getClassLoader());
        } else {
            courseList = null;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(termName);
        if (courseList == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(courseList);
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Term> CREATOR = new Parcelable.Creator<Term>() {
        @Override
        public Term createFromParcel(Parcel in) {
            return new Term(in);
        }

        @Override
        public Term[] newArray(int size) {
            return new Term[size];
        }
    };
}
