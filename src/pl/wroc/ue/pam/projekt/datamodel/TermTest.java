package pl.wroc.ue.pam.projekt.datamodel;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TermTest {

    private Term createTestTerm()
    {
        Term test = new Term("Term 1");
        test.addCourse(new Course("Course 1", 3.5f, 3));
        test.addCourse(new Course("Course 2", 4.0f, 5));
        return test;
    }

    @Test
    public void testArithmeticMean() throws Exception {
        Term test = createTestTerm();
        assertEquals(3.75f, test.arithmeticMean(), 0.01f);
    }

    @Test
    public void testWeightedMean() throws Exception {
        Term test = createTestTerm();
        assertEquals(3.81f, test.weightedMean(), 0.01f);
    }
}