package pl.wroc.ue.pam.projekt.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import pl.wroc.ue.pam.projekt.R;
import pl.wroc.ue.pam.projekt.datamodel.Course;

public class CourseEditDialog extends DialogFragment {

    public interface CourseDialogListener {
        public void onDeleteClick(DialogFragment dialog, Course course);
    }

    CourseDialogListener mListener;

    Course course = null;

    // When editing
    public static CourseEditDialog newInstance(Course course) {
        Bundle args = new Bundle();
        args.putParcelable("course", course);

        CourseEditDialog ced = new CourseEditDialog();
        ced.setArguments(args);

        return ced;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        course = getArguments().getParcelable("course");
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Set layout
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View view = inflater.inflate(R.layout.courseeditdialog, null);
        builder.setView(view);

        // Get controls
        final EditText courseEditText = (EditText) view.findViewById(R.id.courseName2);
        final EditText gradeEditText = (EditText) view.findViewById(R.id.gradeET);
        final EditText ectsEditText = (EditText) view.findViewById(R.id.ectsET);

        // Set buttons
        // TODO: OnClicks to the buttons
        if(course == null) {
            builder.setTitle(getString(R.string.add_course))
                    .setNegativeButton(android.R.string.cancel, null);
        }
        else {
            builder.setTitle(getString(R.string.edit_course))
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            course.setName(courseEditText.getText().toString());
                            course.setGrade(Float.parseFloat(gradeEditText.getText().toString()));
                            course.setEctspoints(Integer.parseInt(ectsEditText.getText().toString()));
                        }
                    })
                    .setNegativeButton(R.string.delete, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            mListener.onDeleteClick(CourseEditDialog.this, course);
                        }
                    })
                    .setNeutralButton(android.R.string.cancel, null);
        }

        Dialog dialog = builder.create();

        // Set values if editing
        if (course != null) {
            courseEditText.setText(course.getName());
            gradeEditText.setText(String.valueOf(course.getGrade()));
            ectsEditText.setText(String.valueOf(course.getEctspoints()));
        }

        return dialog;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (CourseDialogListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement CourseDialogListener");
        }
    }
}